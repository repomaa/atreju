EESchema Schematic File Version 4
LIBS:atreju-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Switch:SW_Push SW2_4
U 1 1 5BDEFC98
P 3450 3350
F 0 "SW2_4" H 3450 3635 50  0000 C CNN
F 1 "SW_Push" H 3450 3544 50  0000 C CNN
F 2 "keyswitches:Kailh_socket_reversible" H 3450 3550 50  0001 C CNN
F 3 "" H 3450 3550 50  0001 C CNN
	1    3450 3350
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW3_0
U 1 1 5BDEFCE6
P 1450 4450
F 0 "SW3_0" H 1450 4735 50  0000 C CNN
F 1 "SW_Push" H 1450 4644 50  0000 C CNN
F 2 "keyswitches:Kailh_socket_reversible" H 1450 4650 50  0001 C CNN
F 3 "" H 1450 4650 50  0001 C CNN
	1    1450 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW2_0
U 1 1 5BDEFD9A
P 1450 3350
F 0 "SW2_0" H 1450 3635 50  0000 C CNN
F 1 "SW_Push" H 1450 3544 50  0000 C CNN
F 2 "keyswitches:Kailh_socket_reversible" H 1450 3550 50  0001 C CNN
F 3 "" H 1450 3550 50  0001 C CNN
	1    1450 3350
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW1_0
U 1 1 5BDEFDCA
P 1450 2250
F 0 "SW1_0" H 1450 2535 50  0000 C CNN
F 1 "SW_Push" H 1450 2444 50  0000 C CNN
F 2 "keyswitches:Kailh_socket_reversible" H 1450 2450 50  0001 C CNN
F 3 "" H 1450 2450 50  0001 C CNN
	1    1450 2250
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW0_0
U 1 1 5BDEFE2A
P 1450 1150
F 0 "SW0_0" H 1450 1435 50  0000 C CNN
F 1 "SW_Push" H 1450 1344 50  0000 C CNN
F 2 "keyswitches:Kailh_socket_reversible" H 1450 1350 50  0001 C CNN
F 3 "" H 1450 1350 50  0001 C CNN
	1    1450 1150
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW1_4
U 1 1 5BDEFF17
P 3450 2250
F 0 "SW1_4" H 3450 2535 50  0000 C CNN
F 1 "SW_Push" H 3450 2444 50  0000 C CNN
F 2 "keyswitches:Kailh_socket_reversible" H 3450 2450 50  0001 C CNN
F 3 "" H 3450 2450 50  0001 C CNN
	1    3450 2250
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW3_1
U 1 1 5BDEFF1D
P 1950 4450
F 0 "SW3_1" H 1950 4735 50  0000 C CNN
F 1 "SW_Push" H 1950 4644 50  0000 C CNN
F 2 "keyswitches:Kailh_socket_reversible" H 1950 4650 50  0001 C CNN
F 3 "" H 1950 4650 50  0001 C CNN
	1    1950 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW2_1
U 1 1 5BDEFF23
P 1950 3350
F 0 "SW2_1" H 1950 3635 50  0000 C CNN
F 1 "SW_Push" H 1950 3544 50  0000 C CNN
F 2 "keyswitches:Kailh_socket_reversible" H 1950 3550 50  0001 C CNN
F 3 "" H 1950 3550 50  0001 C CNN
	1    1950 3350
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW1_1
U 1 1 5BDEFF29
P 1950 2250
F 0 "SW1_1" H 1950 2535 50  0000 C CNN
F 1 "SW_Push" H 1950 2444 50  0000 C CNN
F 2 "keyswitches:Kailh_socket_reversible" H 1950 2450 50  0001 C CNN
F 3 "" H 1950 2450 50  0001 C CNN
	1    1950 2250
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW0_1
U 1 1 5BDEFF2F
P 1950 1150
F 0 "SW0_1" H 1950 1435 50  0000 C CNN
F 1 "SW_Push" H 1950 1344 50  0000 C CNN
F 2 "keyswitches:Kailh_socket_reversible" H 1950 1350 50  0001 C CNN
F 3 "" H 1950 1350 50  0001 C CNN
	1    1950 1150
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW0_4
U 1 1 5BDF06F1
P 3450 1150
F 0 "SW0_4" H 3450 1435 50  0000 C CNN
F 1 "SW_Push" H 3450 1344 50  0000 C CNN
F 2 "keyswitches:Kailh_socket_reversible" H 3450 1350 50  0001 C CNN
F 3 "" H 3450 1350 50  0001 C CNN
	1    3450 1150
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW3_2
U 1 1 5BDF06F7
P 2450 4450
F 0 "SW3_2" H 2450 4735 50  0000 C CNN
F 1 "SW_Push" H 2450 4644 50  0000 C CNN
F 2 "keyswitches:Kailh_socket_reversible" H 2450 4650 50  0001 C CNN
F 3 "" H 2450 4650 50  0001 C CNN
	1    2450 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW2_2
U 1 1 5BDF06FD
P 2450 3350
F 0 "SW2_2" H 2450 3635 50  0000 C CNN
F 1 "SW_Push" H 2450 3544 50  0000 C CNN
F 2 "keyswitches:Kailh_socket_reversible" H 2450 3550 50  0001 C CNN
F 3 "" H 2450 3550 50  0001 C CNN
	1    2450 3350
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW1_2
U 1 1 5BDF0703
P 2450 2250
F 0 "SW1_2" H 2450 2535 50  0000 C CNN
F 1 "SW_Push" H 2450 2444 50  0000 C CNN
F 2 "keyswitches:Kailh_socket_reversible" H 2450 2450 50  0001 C CNN
F 3 "" H 2450 2450 50  0001 C CNN
	1    2450 2250
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW0_2
U 1 1 5BDF0709
P 2450 1150
F 0 "SW0_2" H 2450 1435 50  0000 C CNN
F 1 "SW_Push" H 2450 1344 50  0000 C CNN
F 2 "keyswitches:Kailh_socket_reversible" H 2450 1350 50  0001 C CNN
F 3 "" H 2450 1350 50  0001 C CNN
	1    2450 1150
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW3_5
U 1 1 5BDF070F
P 3950 4450
F 0 "SW3_5" H 3950 4735 50  0000 C CNN
F 1 "SW_Push" H 3950 4644 50  0000 C CNN
F 2 "keyswitches:Kailh_socket_reversible" H 3950 4650 50  0001 C CNN
F 3 "" H 3950 4650 50  0001 C CNN
	1    3950 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW3_3
U 1 1 5BDF0715
P 2950 4450
F 0 "SW3_3" H 2950 4735 50  0000 C CNN
F 1 "SW_Push" H 2950 4644 50  0000 C CNN
F 2 "keyswitches:Kailh_socket_reversible" H 2950 4650 50  0001 C CNN
F 3 "" H 2950 4650 50  0001 C CNN
	1    2950 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW2_3
U 1 1 5BDF071B
P 2950 3350
F 0 "SW2_3" H 2950 3635 50  0000 C CNN
F 1 "SW_Push" H 2950 3544 50  0000 C CNN
F 2 "keyswitches:Kailh_socket_reversible" H 2950 3550 50  0001 C CNN
F 3 "" H 2950 3550 50  0001 C CNN
	1    2950 3350
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW1_3
U 1 1 5BDF0721
P 2950 2250
F 0 "SW1_3" H 2950 2535 50  0000 C CNN
F 1 "SW_Push" H 2950 2444 50  0000 C CNN
F 2 "keyswitches:Kailh_socket_reversible" H 2950 2450 50  0001 C CNN
F 3 "" H 2950 2450 50  0001 C CNN
	1    2950 2250
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW3_4
U 1 1 5BDF0727
P 3450 4450
F 0 "SW3_4" H 3450 4735 50  0000 C CNN
F 1 "SW_Push" H 3450 4644 50  0000 C CNN
F 2 "keyswitches:Kailh_socket_reversible" H 3450 4650 50  0001 C CNN
F 3 "" H 3450 4650 50  0001 C CNN
	1    3450 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW0_3
U 1 1 5BDF0AA2
P 2950 1150
F 0 "SW0_3" H 2950 1435 50  0000 C CNN
F 1 "SW_Push" H 2950 1344 50  0000 C CNN
F 2 "keyswitches:Kailh_socket_reversible" H 2950 1350 50  0001 C CNN
F 3 "" H 2950 1350 50  0001 C CNN
	1    2950 1150
	0    -1   -1   0   
$EndComp
Text GLabel 800  950  0    50   Input ~ 0
R0
Text GLabel 800  2050 0    50   Input ~ 0
R1
Text GLabel 800  3150 0    50   Input ~ 0
R2
Text GLabel 800  4250 0    50   Input ~ 0
R3
Text GLabel 1550 5200 3    50   Input ~ 0
C0
Text GLabel 2050 5200 3    50   Input ~ 0
C1
Text GLabel 2550 5200 3    50   Input ~ 0
C2
Text GLabel 3050 5200 3    50   Input ~ 0
C3
Text GLabel 3550 5200 3    50   Input ~ 0
C4
Wire Wire Line
	800  950  1450 950 
Wire Wire Line
	1450 950  1950 950 
Connection ~ 1450 950 
Wire Wire Line
	1950 950  2450 950 
Connection ~ 1950 950 
Wire Wire Line
	2450 950  2950 950 
Connection ~ 2450 950 
Wire Wire Line
	2950 950  3450 950 
Connection ~ 2950 950 
Wire Wire Line
	1450 1350 1550 1350
Wire Wire Line
	2050 1350 1950 1350
Wire Wire Line
	2450 1350 2550 1350
Wire Wire Line
	3450 1350 3550 1350
Wire Wire Line
	3050 1350 2950 1350
Wire Wire Line
	4050 4650 3950 4650
Wire Wire Line
	800  2050 1450 2050
Wire Wire Line
	1450 2050 1950 2050
Connection ~ 1450 2050
Wire Wire Line
	1950 2050 2450 2050
Connection ~ 1950 2050
Wire Wire Line
	2450 2050 2950 2050
Connection ~ 2450 2050
Wire Wire Line
	2950 2050 3450 2050
Connection ~ 2950 2050
Wire Wire Line
	800  3150 1450 3150
Wire Wire Line
	1450 3150 1950 3150
Connection ~ 1450 3150
Wire Wire Line
	1950 3150 2450 3150
Connection ~ 1950 3150
Wire Wire Line
	2450 3150 2950 3150
Connection ~ 2450 3150
Wire Wire Line
	2950 3150 3450 3150
Connection ~ 2950 3150
Wire Wire Line
	800  4250 1450 4250
Wire Wire Line
	1450 4250 1950 4250
Connection ~ 1450 4250
Wire Wire Line
	1950 4250 2450 4250
Connection ~ 1950 4250
Wire Wire Line
	2450 4250 2950 4250
Connection ~ 2450 4250
Wire Wire Line
	2950 4250 3450 4250
Connection ~ 2950 4250
Wire Wire Line
	3450 4250 3950 4250
Connection ~ 3450 4250
Wire Wire Line
	1450 2450 1550 2450
Wire Wire Line
	1950 2450 2050 2450
Wire Wire Line
	2450 2450 2550 2450
Wire Wire Line
	2950 2450 3050 2450
Wire Wire Line
	3450 2450 3550 2450
Wire Wire Line
	3450 3550 3550 3550
Wire Wire Line
	1450 3550 1550 3550
Wire Wire Line
	1950 3550 2050 3550
Wire Wire Line
	2450 3550 2550 3550
Wire Wire Line
	2950 3550 3050 3550
Wire Wire Line
	1450 4650 1550 4650
Wire Wire Line
	1950 4650 2050 4650
Wire Wire Line
	2450 4650 2550 4650
Wire Wire Line
	2950 4650 3050 4650
Wire Wire Line
	3450 4650 3550 4650
$Comp
L Connector_Generic:Conn_01x08 J2
U 1 1 5BE2134C
P 8300 4950
F 0 "J2" H 8220 4325 50  0000 C CNN
F 1 "TRACKPOINT" H 8220 4416 50  0000 C CNN
F 2 "connectors:8PIN_FFC_VERTICAL" H 8300 4950 50  0001 C CNN
F 3 "~" H 8300 4950 50  0001 C CNN
	1    8300 4950
	-1   0    0    1   
$EndComp
Text GLabel 10300 4450 2    50   Input ~ 0
VCC
Text GLabel 9750 4650 2    50   Input ~ 0
GND
Text GLabel 10300 4750 2    50   Input ~ 0
TPCLK
Text GLabel 10300 5250 2    50   Input ~ 0
TPDATA
Wire Wire Line
	8500 5250 10100 5250
$Comp
L Device:R R7
U 1 1 5BE30328
P 10200 4600
F 0 "R7" H 10270 4646 50  0000 L CNN
F 1 "4k7" H 10270 4555 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 10130 4600 50  0001 C CNN
F 3 "~" H 10200 4600 50  0001 C CNN
	1    10200 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	10200 4750 8500 4750
Wire Wire Line
	8500 4550 9200 4550
Wire Wire Line
	10100 4550 10100 4450
$Comp
L Device:R R6
U 1 1 5BE3D3F2
P 10100 5100
F 0 "R6" H 10170 5146 50  0000 L CNN
F 1 "4k7" H 10170 5055 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 10030 5100 50  0001 C CNN
F 3 "~" H 10100 5100 50  0001 C CNN
	1    10100 5100
	1    0    0    -1  
$EndComp
Connection ~ 10100 5250
Wire Wire Line
	10100 5250 10300 5250
Wire Wire Line
	10100 4950 10100 4550
Connection ~ 10100 4550
$Comp
L Device:R R5
U 1 1 5BE44658
P 9650 5000
F 0 "R5" H 9720 5046 50  0000 L CNN
F 1 "100k" H 9720 4955 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 9580 5000 50  0001 C CNN
F 3 "~" H 9650 5000 50  0001 C CNN
	1    9650 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	9650 4850 9650 4650
Wire Wire Line
	9650 4650 9750 4650
Wire Wire Line
	9650 4650 8500 4650
Connection ~ 9650 4650
$Comp
L Device:C C1
U 1 1 5BE4E098
P 9200 5000
F 0 "C1" H 9315 5046 50  0000 L CNN
F 1 "2.2µF" H 9315 4955 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 9238 4850 50  0001 C CNN
F 3 "~" H 9200 5000 50  0001 C CNN
	1    9200 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	10100 4450 10200 4450
Wire Wire Line
	10300 4750 10200 4750
Connection ~ 10200 4750
Wire Wire Line
	10200 4450 10300 4450
Connection ~ 10200 4450
Connection ~ 9200 5150
Wire Wire Line
	9200 5150 8500 5150
Wire Wire Line
	9200 4850 9200 4550
Connection ~ 9200 4550
Wire Wire Line
	9200 4550 10100 4550
Wire Wire Line
	9650 5150 9200 5150
Text GLabel 8100 3050 2    50   Input ~ 0
VCC
Wire Wire Line
	8000 3500 8100 3500
Wire Wire Line
	8000 3350 8000 3500
Wire Wire Line
	7700 3350 7700 3400
Wire Wire Line
	8100 3050 8000 3050
Wire Wire Line
	7700 3050 8000 3050
Connection ~ 8000 3050
$Comp
L Device:R R4
U 1 1 5BEFD97F
P 8000 3200
F 0 "R4" H 8070 3246 50  0000 L CNN
F 1 "4k7" H 8070 3155 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 7930 3200 50  0001 C CNN
F 3 "~" H 8000 3200 50  0001 C CNN
	1    8000 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 3400 7500 3400
$Comp
L Device:R R3
U 1 1 5BEF675A
P 7700 3200
F 0 "R3" H 7770 3246 50  0000 L CNN
F 1 "4k7" H 7770 3155 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 7630 3200 50  0001 C CNN
F 3 "~" H 7700 3200 50  0001 C CNN
	1    7700 3200
	1    0    0    -1  
$EndComp
Connection ~ 8000 3500
Wire Wire Line
	7500 3500 8000 3500
Connection ~ 7700 3400
Wire Wire Line
	8100 3400 7700 3400
Text GLabel 8100 3400 2    50   Input ~ 0
SCL
Text GLabel 8100 3500 2    50   Input ~ 0
SDA
Text GLabel 6600 5500 0    50   Input ~ 0
GND
$Comp
L Connector:USB_B_Micro J1
U 1 1 5BF5E209
P 4450 950
F 0 "J1" H 4505 1417 50  0000 C CNN
F 1 "USB" H 4505 1326 50  0000 C CNN
F 2 "Connector_USB:USB_Mini-B_AdamTech_MUSB-B5-S-VT-TSMT-1_SMD_Vertical" H 4600 900 50  0001 C CNN
F 3 "~" H 4600 900 50  0001 C CNN
	1    4450 950 
	1    0    0    -1  
$EndComp
Text GLabel 6200 1350 0    50   Input ~ 0
VCC
Text GLabel 5100 950  2    50   Input ~ 0
D+
Text GLabel 5100 1050 2    50   Input ~ 0
D-
Wire Wire Line
	5100 750  4750 750 
Wire Wire Line
	4750 950  5100 950 
Wire Wire Line
	5100 1050 4750 1050
Text GLabel 4450 1450 3    50   Input ~ 0
GND
Wire Wire Line
	4450 1350 4450 1450
NoConn ~ 4750 1150
NoConn ~ 6300 2800
Text GLabel 5650 3200 0    50   Input ~ 0
D+
Text GLabel 5650 3300 0    50   Input ~ 0
D-
$Comp
L Device:R R1
U 1 1 5BF9BF09
P 5900 3200
F 0 "R1" V 5900 3200 50  0000 C CNN
F 1 "22" V 5800 3200 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5830 3200 50  0001 C CNN
F 3 "~" H 5900 3200 50  0001 C CNN
	1    5900 3200
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 5BF9BFE3
P 5900 3300
F 0 "R2" V 5900 3300 50  0000 C CNN
F 1 "22" V 6000 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5830 3300 50  0001 C CNN
F 3 "~" H 5900 3300 50  0001 C CNN
	1    5900 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	6050 3200 6300 3200
Wire Wire Line
	6050 3300 6300 3300
Wire Wire Line
	5750 3300 5650 3300
Wire Wire Line
	5650 3200 5750 3200
NoConn ~ 6300 3000
NoConn ~ 6300 3500
Text GLabel 5100 750  2    50   Input ~ 0
VCC
Text GLabel 7650 2600 2    50   Input ~ 0
R0
Text GLabel 7650 2700 2    50   Input ~ 0
R1
Text GLabel 7650 2800 2    50   Input ~ 0
R2
Text GLabel 7650 2900 2    50   Input ~ 0
R3
Text GLabel 7650 4600 2    50   Input ~ 0
C0
Text GLabel 7650 4700 2    50   Input ~ 0
C1
Text GLabel 7650 4800 2    50   Input ~ 0
C2
Text GLabel 7650 4900 2    50   Input ~ 0
C3
Text GLabel 7650 5000 2    50   Input ~ 0
C4
Text GLabel 7650 4000 2    50   Input ~ 0
TPCLK
Text GLabel 7650 4100 2    50   Input ~ 0
TPDATA
Wire Wire Line
	7500 5000 7650 5000
Wire Wire Line
	7650 5100 7500 5100
Wire Wire Line
	7500 4000 7650 4000
Wire Wire Line
	7650 4900 7500 4900
Wire Wire Line
	7500 4800 7650 4800
Wire Wire Line
	7650 4700 7500 4700
Wire Wire Line
	7500 4600 7650 4600
Wire Wire Line
	7650 2900 7500 2900
Wire Wire Line
	7500 2800 7650 2800
Wire Wire Line
	7650 2700 7500 2700
Wire Wire Line
	7500 2600 7650 2600
NoConn ~ 7500 4400
NoConn ~ 7500 4300
NoConn ~ 7500 3800
NoConn ~ 7500 3200
NoConn ~ 7500 3100
Wire Wire Line
	7000 1900 6900 1900
Connection ~ 6900 1900
$Comp
L Connector:AudioJack4_Ground J3
U 1 1 5C03B98D
P 9000 1000
F 0 "J3" H 8966 1342 50  0000 C CNN
F 1 "I2C CONN" H 8966 1251 50  0000 C CNN
F 2 "connectors:LUM 1503-13V" H 9000 1000 50  0001 C CNN
F 3 "~" H 9000 1000 50  0001 C CNN
	1    9000 1000
	1    0    0    -1  
$EndComp
Text GLabel 9350 900  2    50   Input ~ 0
GND
Text GLabel 9350 1000 2    50   Input ~ 0
SDA
Text GLabel 9350 1100 2    50   Input ~ 0
SCL
Text GLabel 9350 1200 2    50   Input ~ 0
VCC
Wire Wire Line
	9350 900  9200 900 
Wire Wire Line
	9200 1000 9350 1000
Wire Wire Line
	9350 1100 9200 1100
Wire Wire Line
	9200 1200 9350 1200
Text GLabel 4050 5200 3    50   Input ~ 0
C5
$Comp
L Device:D D21
U 1 1 5C065246
P 4050 4900
F 0 "D21" V 4096 4821 50  0000 R CNN
F 1 "D" V 4005 4821 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4050 4900 50  0001 C CNN
F 3 "~" H 4050 4900 50  0001 C CNN
	1    4050 4900
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D20
U 1 1 5C065DFD
P 3550 4900
F 0 "D20" V 3596 4821 50  0000 R CNN
F 1 "D" V 3505 4821 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3550 4900 50  0001 C CNN
F 3 "~" H 3550 4900 50  0001 C CNN
	1    3550 4900
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D16
U 1 1 5C065E69
P 3050 4900
F 0 "D16" V 3096 4821 50  0000 R CNN
F 1 "D" V 3005 4821 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3050 4900 50  0001 C CNN
F 3 "~" H 3050 4900 50  0001 C CNN
	1    3050 4900
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D12
U 1 1 5C065ECD
P 2550 4900
F 0 "D12" V 2596 4821 50  0000 R CNN
F 1 "D" V 2505 4821 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2550 4900 50  0001 C CNN
F 3 "~" H 2550 4900 50  0001 C CNN
	1    2550 4900
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D8
U 1 1 5C065F37
P 2050 4900
F 0 "D8" V 2096 4821 50  0000 R CNN
F 1 "D" V 2005 4821 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2050 4900 50  0001 C CNN
F 3 "~" H 2050 4900 50  0001 C CNN
	1    2050 4900
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D4
U 1 1 5C066024
P 1550 4900
F 0 "D4" V 1596 4821 50  0000 R CNN
F 1 "D" V 1505 4821 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1550 4900 50  0001 C CNN
F 3 "~" H 1550 4900 50  0001 C CNN
	1    1550 4900
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D3
U 1 1 5C066094
P 1550 3800
F 0 "D3" V 1596 3721 50  0000 R CNN
F 1 "D" V 1505 3721 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1550 3800 50  0001 C CNN
F 3 "~" H 1550 3800 50  0001 C CNN
	1    1550 3800
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D7
U 1 1 5C066110
P 2050 3800
F 0 "D7" V 2096 3721 50  0000 R CNN
F 1 "D" V 2005 3721 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2050 3800 50  0001 C CNN
F 3 "~" H 2050 3800 50  0001 C CNN
	1    2050 3800
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D11
U 1 1 5C066182
P 2550 3800
F 0 "D11" V 2596 3721 50  0000 R CNN
F 1 "D" V 2505 3721 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2550 3800 50  0001 C CNN
F 3 "~" H 2550 3800 50  0001 C CNN
	1    2550 3800
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D15
U 1 1 5C0661F8
P 3050 3800
F 0 "D15" V 3096 3721 50  0000 R CNN
F 1 "D" V 3005 3721 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3050 3800 50  0001 C CNN
F 3 "~" H 3050 3800 50  0001 C CNN
	1    3050 3800
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D19
U 1 1 5C066270
P 3550 3800
F 0 "D19" V 3596 3721 50  0000 R CNN
F 1 "D" V 3505 3721 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3550 3800 50  0001 C CNN
F 3 "~" H 3550 3800 50  0001 C CNN
	1    3550 3800
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D18
U 1 1 5C0663A0
P 3550 2750
F 0 "D18" V 3596 2671 50  0000 R CNN
F 1 "D" V 3505 2671 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3550 2750 50  0001 C CNN
F 3 "~" H 3550 2750 50  0001 C CNN
	1    3550 2750
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D14
U 1 1 5C06642A
P 3050 2750
F 0 "D14" V 3096 2671 50  0000 R CNN
F 1 "D" V 3005 2671 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3050 2750 50  0001 C CNN
F 3 "~" H 3050 2750 50  0001 C CNN
	1    3050 2750
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D10
U 1 1 5C0664A6
P 2550 2750
F 0 "D10" V 2596 2671 50  0000 R CNN
F 1 "D" V 2505 2671 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2550 2750 50  0001 C CNN
F 3 "~" H 2550 2750 50  0001 C CNN
	1    2550 2750
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D6
U 1 1 5C066522
P 2050 2750
F 0 "D6" V 2096 2671 50  0000 R CNN
F 1 "D" V 2005 2671 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2050 2750 50  0001 C CNN
F 3 "~" H 2050 2750 50  0001 C CNN
	1    2050 2750
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D2
U 1 1 5C0665A2
P 1550 2750
F 0 "D2" V 1596 2671 50  0000 R CNN
F 1 "D" V 1505 2671 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1550 2750 50  0001 C CNN
F 3 "~" H 1550 2750 50  0001 C CNN
	1    1550 2750
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D1
U 1 1 5C066628
P 1550 1650
F 0 "D1" V 1596 1571 50  0000 R CNN
F 1 "D" V 1505 1571 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1550 1650 50  0001 C CNN
F 3 "~" H 1550 1650 50  0001 C CNN
	1    1550 1650
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D5
U 1 1 5C0666BC
P 2050 1650
F 0 "D5" V 2096 1571 50  0000 R CNN
F 1 "D" V 2005 1571 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2050 1650 50  0001 C CNN
F 3 "~" H 2050 1650 50  0001 C CNN
	1    2050 1650
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D9
U 1 1 5C066746
P 2550 1650
F 0 "D9" V 2596 1571 50  0000 R CNN
F 1 "D" V 2505 1571 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2550 1650 50  0001 C CNN
F 3 "~" H 2550 1650 50  0001 C CNN
	1    2550 1650
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D13
U 1 1 5C0667D2
P 3050 1650
F 0 "D13" V 3096 1571 50  0000 R CNN
F 1 "D" V 3005 1571 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3050 1650 50  0001 C CNN
F 3 "~" H 3050 1650 50  0001 C CNN
	1    3050 1650
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D17
U 1 1 5C066864
P 3550 1650
F 0 "D17" V 3596 1571 50  0000 R CNN
F 1 "D" V 3505 1571 50  0000 R CNN
F 2 "Diode_SMD:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3550 1650 50  0001 C CNN
F 3 "~" H 3550 1650 50  0001 C CNN
	1    3550 1650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1550 1350 1550 1500
Wire Wire Line
	2050 1350 2050 1500
Wire Wire Line
	2550 1350 2550 1500
Wire Wire Line
	3050 1350 3050 1500
Wire Wire Line
	3550 1350 3550 1500
Wire Wire Line
	1550 2450 1550 2600
Wire Wire Line
	2050 2450 2050 2600
Wire Wire Line
	2550 2450 2550 2600
Wire Wire Line
	3050 2450 3050 2600
Wire Wire Line
	3550 2450 3550 2600
Wire Wire Line
	1550 3550 1550 3650
Wire Wire Line
	2050 3550 2050 3650
Wire Wire Line
	2550 3550 2550 3650
Wire Wire Line
	3050 3550 3050 3650
Wire Wire Line
	3550 3550 3550 3650
Wire Wire Line
	3550 4650 3550 4750
Wire Wire Line
	4050 4650 4050 4750
Wire Wire Line
	3050 4750 3050 4650
Wire Wire Line
	2550 4650 2550 4750
Wire Wire Line
	2050 4650 2050 4750
Wire Wire Line
	1550 4650 1550 4750
Wire Wire Line
	4050 5050 4050 5200
Wire Wire Line
	3550 5200 3550 5100
Wire Wire Line
	3050 5050 3050 5100
Wire Wire Line
	2550 5200 2550 5100
Wire Wire Line
	2050 5050 2050 5100
Wire Wire Line
	1550 5200 1550 5100
Text GLabel 7650 5100 2    50   Input ~ 0
C5
Wire Wire Line
	7650 4100 7500 4100
Wire Wire Line
	6800 1900 6900 1900
$Comp
L Device:R 10k1
U 1 1 5BE28F6E
P 1000 5750
F 0 "10k1" V 1000 5750 50  0000 C CNN
F 1 "22" V 900 5750 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 930 5750 50  0001 C CNN
F 3 "~" H 1000 5750 50  0001 C CNN
	1    1000 5750
	-1   0    0    1   
$EndComp
Wire Wire Line
	1000 6150 1000 5900
Text GLabel 1000 5500 1    50   Input ~ 0
VCC
Wire Wire Line
	1000 5500 1000 5600
Wire Wire Line
	1550 1800 1650 1800
Wire Wire Line
	1650 1800 1650 2900
Wire Wire Line
	1650 5100 1550 5100
Connection ~ 1550 5100
Wire Wire Line
	1550 5100 1550 5050
Wire Wire Line
	1550 3950 1650 3950
Connection ~ 1650 3950
Wire Wire Line
	1650 3950 1650 5100
Wire Wire Line
	1550 2900 1650 2900
Connection ~ 1650 2900
Wire Wire Line
	1650 2900 1650 3950
Wire Wire Line
	2050 1800 2150 1800
Wire Wire Line
	2150 1800 2150 2900
Wire Wire Line
	2150 5100 2050 5100
Connection ~ 2050 5100
Wire Wire Line
	2050 5100 2050 5200
Wire Wire Line
	2050 3950 2150 3950
Connection ~ 2150 3950
Wire Wire Line
	2150 3950 2150 5100
Wire Wire Line
	2050 2900 2150 2900
Connection ~ 2150 2900
Wire Wire Line
	2150 2900 2150 3950
Wire Wire Line
	2550 1800 2650 1800
Wire Wire Line
	2650 1800 2650 2900
Wire Wire Line
	2650 5100 2550 5100
Connection ~ 2550 5100
Wire Wire Line
	2550 5100 2550 5050
Wire Wire Line
	2550 3950 2650 3950
Connection ~ 2650 3950
Wire Wire Line
	2650 3950 2650 5100
Wire Wire Line
	2550 2900 2650 2900
Connection ~ 2650 2900
Wire Wire Line
	2650 2900 2650 3950
Wire Wire Line
	3050 1800 3150 1800
Wire Wire Line
	3150 1800 3150 2900
Wire Wire Line
	3150 5100 3050 5100
Connection ~ 3050 5100
Wire Wire Line
	3050 5100 3050 5200
Wire Wire Line
	3050 3950 3150 3950
Connection ~ 3150 3950
Wire Wire Line
	3150 3950 3150 5100
Wire Wire Line
	3050 2900 3150 2900
Connection ~ 3150 2900
Wire Wire Line
	3150 2900 3150 3950
Wire Wire Line
	3550 1800 3650 1800
Wire Wire Line
	3650 1800 3650 2900
Wire Wire Line
	3650 5100 3550 5100
Connection ~ 3550 5100
Wire Wire Line
	3550 5100 3550 5050
Wire Wire Line
	3550 3950 3650 3950
Connection ~ 3650 3950
Wire Wire Line
	3650 3950 3650 5100
Wire Wire Line
	3550 2900 3650 2900
Connection ~ 3650 2900
Wire Wire Line
	3650 2900 3650 3950
$Comp
L Device:Crystal Y1
U 1 1 5BE35D8E
P 6000 2550
F 0 "Y1" V 5954 2681 50  0000 L CNN
F 1 "Crystal" V 6045 2681 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_HC49-SD_HandSoldering" H 6000 2550 50  0001 C CNN
F 3 "~" H 6000 2550 50  0001 C CNN
	1    6000 2550
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 5BE35F8C
P 5700 2400
F 0 "C2" V 5448 2400 50  0000 C CNN
F 1 "C" V 5539 2400 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5738 2250 50  0001 C CNN
F 3 "~" H 5700 2400 50  0001 C CNN
	1    5700 2400
	0    1    1    0   
$EndComp
$Comp
L Device:C C3
U 1 1 5BE360A5
P 5700 2700
F 0 "C3" V 5448 2700 50  0000 C CNN
F 1 "C" V 5539 2700 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5738 2550 50  0001 C CNN
F 3 "~" H 5700 2700 50  0001 C CNN
	1    5700 2700
	0    1    1    0   
$EndComp
Text GLabel 5450 2850 3    50   Input ~ 0
GND
Wire Wire Line
	6300 2400 6000 2400
Wire Wire Line
	6000 2400 5850 2400
Connection ~ 6000 2400
Wire Wire Line
	5550 2400 5450 2400
Wire Wire Line
	5450 2400 5450 2700
Wire Wire Line
	5550 2700 5450 2700
Connection ~ 5450 2700
Wire Wire Line
	5450 2700 5450 2850
Wire Wire Line
	5850 2700 6000 2700
Wire Wire Line
	6000 2700 6300 2700
Wire Wire Line
	6300 2700 6300 2600
Connection ~ 6000 2700
Text GLabel 7750 5900 0    50   Input ~ 0
MOUSE1
Text GLabel 7750 6000 0    50   Input ~ 0
MOUSE2
Text GLabel 7750 6100 0    50   Input ~ 0
MOUSE3
Text GLabel 8650 5050 2    50   Input ~ 0
MOUSE1
Text GLabel 8650 4950 2    50   Input ~ 0
MOUSE2
Text GLabel 8650 4850 2    50   Input ~ 0
MOUSE3
Wire Wire Line
	8650 4850 8500 4850
Wire Wire Line
	8500 4950 8650 4950
Wire Wire Line
	8650 5050 8500 5050
$Comp
L Connector:Conn_01x04_Female J4
U 1 1 5BEDC459
P 9300 5900
F 0 "J4" H 9328 5876 50  0000 L CNN
F 1 "MOUSE BTNS" H 9328 5785 50  0000 L CNN
F 2 "Connector_Molex:Molex_PicoBlade_53048-0810_1x04_P1.25mm_Horizontal" H 9300 5900 50  0001 C CNN
F 3 "~" H 9300 5900 50  0001 C CNN
	1    9300 5900
	1    0    0    -1  
$EndComp
Text GLabel 8900 5800 0    50   Input ~ 0
VCC
$Comp
L Device:R R8
U 1 1 5BEDC971
P 7900 5750
F 0 "R8" V 7693 5750 50  0000 C CNN
F 1 "10k" V 7784 5750 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 7830 5750 50  0001 C CNN
F 3 "~" H 7900 5750 50  0001 C CNN
	1    7900 5750
	-1   0    0    1   
$EndComp
Wire Wire Line
	9100 5800 8900 5800
Text GLabel 7750 5600 0    50   Input ~ 0
GND
$Comp
L Device:R R9
U 1 1 5BF76C77
P 8200 5850
F 0 "R9" V 7993 5850 50  0000 C CNN
F 1 "10k" V 8084 5850 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 8130 5850 50  0001 C CNN
F 3 "~" H 8200 5850 50  0001 C CNN
	1    8200 5850
	-1   0    0    1   
$EndComp
$Comp
L Device:R R10
U 1 1 5BF76D07
P 8500 5950
F 0 "R10" V 8293 5950 50  0000 C CNN
F 1 "10k" V 8384 5950 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 8430 5950 50  0001 C CNN
F 3 "~" H 8500 5950 50  0001 C CNN
	1    8500 5950
	-1   0    0    1   
$EndComp
Wire Wire Line
	9100 6100 8500 6100
Wire Wire Line
	9100 6000 8200 6000
Wire Wire Line
	9100 5900 7900 5900
Connection ~ 7900 5900
Wire Wire Line
	7900 5900 7750 5900
Connection ~ 8200 6000
Wire Wire Line
	8200 6000 7750 6000
Connection ~ 8500 6100
Wire Wire Line
	8500 6100 7750 6100
Wire Wire Line
	8500 5800 8500 5600
Wire Wire Line
	8500 5600 8200 5600
Connection ~ 7900 5600
Wire Wire Line
	7900 5600 7750 5600
Wire Wire Line
	8200 5700 8200 5600
Connection ~ 8200 5600
Wire Wire Line
	8200 5600 7900 5600
Wire Wire Line
	4350 1350 4450 1350
Connection ~ 4450 1350
Text GLabel 6200 1650 0    50   Input ~ 0
GND
$Comp
L Device:C C7
U 1 1 5C0CCA85
P 6700 1500
F 0 "C7" H 6585 1454 50  0000 R CNN
F 1 "C" H 6585 1545 50  0000 R CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6738 1350 50  0001 C CNN
F 3 "~" H 6700 1500 50  0001 C CNN
	1    6700 1500
	-1   0    0    1   
$EndComp
$Comp
L Device:CP C4
U 1 1 5C100981
P 6350 1500
F 0 "C4" H 6468 1546 50  0000 L CNN
F 1 "CP" H 6468 1455 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x5.4" H 6388 1350 50  0001 C CNN
F 3 "~" H 6350 1500 50  0001 C CNN
	1    6350 1500
	1    0    0    -1  
$EndComp
Connection ~ 6350 1650
Wire Wire Line
	6350 1650 6200 1650
Wire Wire Line
	6200 1350 6350 1350
Connection ~ 6350 1350
Connection ~ 6700 1350
Wire Wire Line
	6700 1350 6900 1350
Wire Wire Line
	6900 1350 6900 1900
Text GLabel 6300 2200 0    50   Input ~ 0
RST
Text GLabel 700  6150 0    50   Input ~ 0
RST
Text GLabel 1000 7350 3    50   Input ~ 0
GND
Wire Wire Line
	700  6150 1000 6150
Wire Wire Line
	1000 6300 1000 6150
Connection ~ 1000 6150
$Comp
L Switch:SW_Push SW1
U 1 1 5C1D31D1
P 1000 6500
F 0 "SW1" V 1046 6452 50  0000 R CNN
F 1 "RST_BTN" V 955 6452 50  0000 R CNN
F 2 "Button_Switch_SMD:SW_SPST_B3S-1000" H 1000 6700 50  0001 C CNN
F 3 "" H 1000 6700 50  0001 C CNN
	1    1000 6500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1000 7350 1000 7250
Text GLabel 7650 2300 2    50   Input ~ 0
SCK
Text GLabel 7650 2500 2    50   Input ~ 0
MISO
Text GLabel 7650 2400 2    50   Input ~ 0
MOSI
Wire Wire Line
	7650 2500 7500 2500
Wire Wire Line
	7500 2400 7650 2400
Wire Wire Line
	7650 2300 7500 2300
Text GLabel 7650 3900 2    50   Input ~ 0
TXLED
Wire Wire Line
	7650 3900 7500 3900
Text GLabel 7650 2200 2    50   Input ~ 0
RXLED
Wire Wire Line
	7650 2200 7500 2200
Wire Wire Line
	6800 5500 6600 5500
Wire Wire Line
	6900 5500 6800 5500
Connection ~ 6800 5500
$Comp
L MCU_Microchip_ATmega:ATmega32U4-AU U1
U 1 1 5BF42982
P 6900 3700
F 0 "U1" H 6900 1814 50  0000 C CNN
F 1 "ATmega32U4-AU" H 6900 1723 50  0000 C CNN
F 2 "Package_QFP:TQFP-44_10x10mm_P0.8mm" H 6900 3700 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7766-8-bit-AVR-ATmega16U4-32U4_Datasheet.pdf" H 6900 3700 50  0001 C CNN
	1    6900 3700
	1    0    0    -1  
$EndComp
NoConn ~ 7500 3700
NoConn ~ 7500 3600
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J5
U 1 1 5C2D0D7C
P 1400 6050
F 0 "J5" H 1450 5850 50  0000 C CNN
F 1 "ICSP" H 1450 5750 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x03_P2.54mm_Horizontal" H 1400 6050 50  0001 C CNN
F 3 "~" H 1400 6050 50  0001 C CNN
	1    1400 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 6150 1200 6150
Text GLabel 1900 5850 2    50   Input ~ 0
MISO
Text GLabel 1900 5750 2    50   Input ~ 0
SCK
Wire Wire Line
	1200 6050 1150 6050
Wire Wire Line
	1200 5850 1200 5950
Text GLabel 1900 5950 2    50   Input ~ 0
VCC
Text GLabel 1900 6150 2    50   Input ~ 0
GND
Text GLabel 1900 6050 2    50   Input ~ 0
MOSI
Wire Wire Line
	1700 6150 1900 6150
Wire Wire Line
	1900 6050 1700 6050
Wire Wire Line
	1700 5950 1900 5950
$Comp
L Device:LED D23
U 1 1 5C35179C
P 1550 7250
F 0 "D23" H 1541 7466 50  0000 C CNN
F 1 "RX_LED" H 1541 7375 50  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1550 7250 50  0001 C CNN
F 3 "~" H 1550 7250 50  0001 C CNN
	1    1550 7250
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D22
U 1 1 5C351A82
P 1550 6900
F 0 "D22" H 1541 7116 50  0000 C CNN
F 1 "TX_LED" H 1541 7025 50  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1550 6900 50  0001 C CNN
F 3 "~" H 1550 6900 50  0001 C CNN
	1    1550 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R11
U 1 1 5C351B9E
P 1200 6900
F 0 "R11" V 993 6900 50  0000 C CNN
F 1 "R" V 1084 6900 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 1130 6900 50  0001 C CNN
F 3 "~" H 1200 6900 50  0001 C CNN
	1    1200 6900
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 5C351CB3
P 1200 7250
F 0 "R12" V 993 7250 50  0000 C CNN
F 1 "R" V 1084 7250 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 1130 7250 50  0001 C CNN
F 3 "~" H 1200 7250 50  0001 C CNN
	1    1200 7250
	0    1    1    0   
$EndComp
Text GLabel 1750 6900 2    50   Input ~ 0
TXLED
Text GLabel 1750 7250 2    50   Input ~ 0
RXLED
Wire Wire Line
	1750 6900 1700 6900
Wire Wire Line
	1700 7250 1750 7250
Wire Wire Line
	1400 7250 1350 7250
Wire Wire Line
	1400 6900 1350 6900
Wire Wire Line
	1050 6900 1000 6900
Connection ~ 1000 6900
Wire Wire Line
	1000 6900 1000 6700
Wire Wire Line
	1050 7250 1000 7250
Connection ~ 1000 7250
Wire Wire Line
	1000 7250 1000 6900
Wire Wire Line
	1900 5850 1200 5850
Wire Wire Line
	1900 5750 1150 5750
Wire Wire Line
	1150 5750 1150 6050
Wire Wire Line
	6350 1650 6700 1650
Wire Wire Line
	6350 1350 6700 1350
$Comp
L Connector:AudioJack4_Ground J6
U 1 1 5C7B7175
P 9950 1000
F 0 "J6" H 9916 1342 50  0000 C CNN
F 1 "I2C CONN" H 9916 1251 50  0000 C CNN
F 2 "connectors:LUM 1503-13V" H 9950 1000 50  0001 C CNN
F 3 "~" H 9950 1000 50  0001 C CNN
	1    9950 1000
	1    0    0    -1  
$EndComp
Text GLabel 10300 900  2    50   Input ~ 0
GND
Text GLabel 10300 1000 2    50   Input ~ 0
SDA
Text GLabel 10300 1100 2    50   Input ~ 0
SCL
Text GLabel 10300 1200 2    50   Input ~ 0
VCC
Wire Wire Line
	10300 900  10150 900 
Wire Wire Line
	10150 1000 10300 1000
Wire Wire Line
	10300 1100 10150 1100
Wire Wire Line
	10150 1200 10300 1200
$EndSCHEMATC
